From f3a33e9bd1bfeefe97e268ab335bb80ccc3e8ef3 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Jonas=20=C3=85dahl?= <jadahl@gmail.com>
Date: Fri, 20 Oct 2023 15:44:29 +0800
Subject: [PATCH 1/4] x11-display: Make subwindow redirection call mode
 specific

This means that for X11 sessions we'll do it before any windows are
mapped, and before any plugin implementation is started. Doing it before
a plugin is started is important, because things that the plugin does
during startup can have consequences on how compositing on Xorg works.

For the Xwayland case, we'll do it relatively in the setup phase. It
appears to have been harmless to do it later in the post-opened signal,
but there is no harm in doing it as one of the earlier steps.

Closes: https://gitlab.gnome.org/GNOME/mutter/-/issues/3089
Part-of: <https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/3329>
---
 src/compositor/meta-compositor-x11.c | 2 ++
 src/wayland/meta-xwayland.c          | 1 +
 src/x11/meta-x11-display.c           | 1 -
 3 files changed, 3 insertions(+), 1 deletion(-)

diff --git a/src/compositor/meta-compositor-x11.c b/src/compositor/meta-compositor-x11.c
index 657f9bdb65c..ea06c85fd30 100644
--- a/src/compositor/meta-compositor-x11.c
+++ b/src/compositor/meta-compositor-x11.c
@@ -190,6 +190,8 @@ meta_compositor_x11_manage (MetaCompositor  *compositor,
 
   compositor_x11->have_x11_sync_object = meta_sync_ring_init (cogl_context, xdisplay);
 
+  meta_x11_display_redirect_windows (x11_display, display);
+
   return TRUE;
 }
 
diff --git a/src/wayland/meta-xwayland.c b/src/wayland/meta-xwayland.c
index 3366f11adc7..db0a4a271a4 100644
--- a/src/wayland/meta-xwayland.c
+++ b/src/wayland/meta-xwayland.c
@@ -1179,6 +1179,7 @@ on_x11_display_setup (MetaDisplay         *display,
 {
   MetaX11Display *x11_display = meta_display_get_x11_display (display);
 
+  meta_x11_display_redirect_windows (x11_display, display);
   meta_xwayland_init_dnd (x11_display);
   meta_xwayland_init_xrandr (manager, x11_display);
 }
diff --git a/src/x11/meta-x11-display.c b/src/x11/meta-x11-display.c
index 6b8d7faf556..6de0511dc67 100644
--- a/src/x11/meta-x11-display.c
+++ b/src/x11/meta-x11-display.c
@@ -308,7 +308,6 @@ on_x11_display_opened (MetaX11Display *x11_display,
                        MetaDisplay    *display)
 {
   meta_display_manage_all_xwindows (display);
-  meta_x11_display_redirect_windows (x11_display, display);
 }
 
 static void
-- 
GitLab


From 2e2dfc0bf5a712bfd1a9da1b2a821ef293bf9881 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Jonas=20=C3=85dahl?= <jadahl@gmail.com>
Date: Fri, 20 Oct 2023 17:03:31 +0800
Subject: [PATCH 2/4] display: Move X11 initial focus handling to
 MetaX11Display

It's X11 specific, so put it in the X11 display manager object.

Part-of: <https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/3329>
---
 src/core/display.c         | 34 ----------------------------------
 src/x11/meta-x11-display.c | 25 +++++++++++++++++++++++++
 2 files changed, 25 insertions(+), 34 deletions(-)

diff --git a/src/core/display.c b/src/core/display.c
index 8c036462156..5b8bb82167d 100644
--- a/src/core/display.c
+++ b/src/core/display.c
@@ -945,9 +945,6 @@ meta_display_new (MetaContext  *context,
   MetaDisplay *display;
   MetaDisplayPrivate *priv;
   guint32 timestamp;
-#ifdef HAVE_X11_CLIENT
-  Window old_active_xwindow = None;
-#endif
   MetaMonitorManager *monitor_manager;
   MetaSettings *settings;
   MetaInputCapture *input_capture;
@@ -1066,14 +1063,6 @@ meta_display_new (MetaContext  *context,
   display->last_focus_time = timestamp;
   display->last_user_time = timestamp;
 
-#ifdef HAVE_X11
-  if (!meta_is_wayland_compositor ())
-    meta_prop_get_window (display->x11_display,
-                          display->x11_display->xroot,
-                          display->x11_display->atom__NET_ACTIVE_WINDOW,
-                          &old_active_xwindow);
-#endif
-
   if (!meta_compositor_manage (display->compositor, error))
     {
       g_object_unref (display);
@@ -1094,30 +1083,7 @@ meta_display_new (MetaContext  *context,
   g_signal_connect (display->gesture_tracker, "state-changed",
                     G_CALLBACK (gesture_tracker_state_changed), display);
 
-  /* We know that if mutter is running as a Wayland compositor,
-   * we start out with no windows.
-   */
-#ifdef HAVE_X11_CLIENT
-  if (!meta_is_wayland_compositor ())
-    meta_display_manage_all_xwindows (display);
-
-  if (old_active_xwindow != None)
-    {
-      MetaWindow *old_active_window;
-      old_active_window = meta_x11_display_lookup_x_window (display->x11_display,
-                                                            old_active_xwindow);
-      if (old_active_window)
-        meta_window_focus (old_active_window, timestamp);
-      else
-        meta_display_unset_input_focus (display, timestamp);
-    }
-  else
-    {
-      meta_display_unset_input_focus (display, timestamp);
-    }
-#else
   meta_display_unset_input_focus (display, timestamp);
-#endif
 
   g_signal_connect (stage, "notify::is-grabbed",
                     G_CALLBACK (on_is_grabbed_changed), display);
diff --git a/src/x11/meta-x11-display.c b/src/x11/meta-x11-display.c
index 6de0511dc67..267fa1b8355 100644
--- a/src/x11/meta-x11-display.c
+++ b/src/x11/meta-x11-display.c
@@ -307,7 +307,32 @@ static void
 on_x11_display_opened (MetaX11Display *x11_display,
                        MetaDisplay    *display)
 {
+  Window old_active_xwindow = None;
+
+  if (!meta_is_wayland_compositor ())
+    {
+      meta_prop_get_window (display->x11_display,
+                            display->x11_display->xroot,
+                            display->x11_display->atom__NET_ACTIVE_WINDOW,
+                            &old_active_xwindow);
+    }
+
   meta_display_manage_all_xwindows (display);
+
+  if (old_active_xwindow != None)
+    {
+      MetaWindow *old_active_window;
+
+      old_active_window = meta_x11_display_lookup_x_window (x11_display,
+                                                            old_active_xwindow);
+      if (old_active_window)
+        {
+          uint32_t timestamp;
+
+          timestamp = display->x11_display->timestamp;
+          meta_window_focus (old_active_window, timestamp);
+        }
+    }
 }
 
 static void
-- 
GitLab


From 93d1eb01a6e1dd1ccae46cfb3a4c0aa6caa17eb5 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Jonas=20=C3=85dahl?= <jadahl@gmail.com>
Date: Tue, 17 Oct 2023 15:46:00 +0800
Subject: [PATCH 3/4] tests/x11: Fix replace test to catch the second instance
 failing

The test never noticed that the second instance never actually managed
to load; it was looping a multi second retry session trying to redirect
windows, meaning it failed to catch https://gitlab.gnome.org/GNOME/mutter/-/issues/3089.

Fix the test so that it always waits for mutter to finish loading
successfully, just like it waits fro the first.

Part-of: <https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/3329>
---
 src/tests/x11-test.sh | 3 +++
 1 file changed, 3 insertions(+)

diff --git a/src/tests/x11-test.sh b/src/tests/x11-test.sh
index 59e460fc336..d95b2460f6e 100755
--- a/src/tests/x11-test.sh
+++ b/src/tests/x11-test.sh
@@ -34,6 +34,9 @@ echo \# Launched with pid $MUTTER2_PID
 MUTTER2_PID=$!
 wait $MUTTER1_PID
 
+echo \# Waiting for the second mutter to finish loading
+gdbus wait --session org.gnome.Mutter.IdleMonitor
+
 sleep 2
 
 echo \# Terminating clients > /dev/stderr
-- 
GitLab


From a68385a179ca31d9aec191ad8255ae16ade8b134 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Jonas=20=C3=85dahl?= <jadahl@gmail.com>
Date: Mon, 23 Oct 2023 14:47:33 +0800
Subject: [PATCH 4/4] display: Rename mandatory X11 initialization function

Simply to make it clear that the renamed function is specific to a
particular X11 initialization mode (mandatory Xwayland), put that in the
name, so that it's easier to understand when this function is relevant.

Part-of: <https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/3329>
---
 src/core/display.c | 8 ++++----
 1 file changed, 4 insertions(+), 4 deletions(-)

diff --git a/src/core/display.c b/src/core/display.c
index 5b8bb82167d..8a86d3736bb 100644
--- a/src/core/display.c
+++ b/src/core/display.c
@@ -910,9 +910,9 @@ meta_display_init_x11 (MetaDisplay         *display,
 }
 
 static void
-on_x11_initialized (MetaDisplay  *display,
-                    GAsyncResult *result,
-                    gpointer      user_data)
+on_mandatory_x11_initialized (MetaDisplay  *display,
+                              GAsyncResult *result,
+                              gpointer      user_data)
 {
   g_autoptr (GError) error = NULL;
 
@@ -1036,7 +1036,7 @@ meta_display_new (MetaContext  *context,
       if (x11_display_policy == META_X11_DISPLAY_POLICY_MANDATORY)
         {
           meta_display_init_x11 (display, NULL,
-                                 (GAsyncReadyCallback) on_x11_initialized,
+                                 (GAsyncReadyCallback) on_mandatory_x11_initialized,
                                  NULL);
         }
 #endif /* HAVE_XWAYLAND */
-- 
GitLab

